provider "aws"  {}

variable "cidr_blocks" {
    description = "cidr blocks"
    type = list(string)  
}
resource "aws_vpc" "development-vpc" {
    cidr_block = var.cidr_blocks[0]
    tags = {
         Name: "Development"
         vpc_evn : "dev"
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.cidr_blocks[1]
    availability_zone = "us-east-1a"
    tags = {
         Name : "subnet-1-dev"
    }
}

resource "aws_instance" "terraform_example" {
    ami = "ami-09025f5e3f0599168"
    instance_type = "t2.micro"
}
